import React from 'react';

import cv1 from '../assets/cv1.jpg'
import './styles/ProfilePicture.css'
function ProfilePicture() {
    return (
        <img src={cv1} alt="" className='ProfilePicture_cv'/>
    );
}

export default ProfilePicture; 