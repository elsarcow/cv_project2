import React from 'react';
import './styles/NavBar.css'
import {Link} from 'react-router-dom'
function NavBar(){
    return (
     <div className='Navbar'>
         <Link to='/' className='Navbar-name btn'>Carlos Muñoz</Link>
        <div className="Navbar-menu">
            <Link to='/bio' className='Navbar-menu_btn btn btn-info'>Bio</Link>
            <Link to='/skills' className='Navbar-menu_btn btn btn-info'>Skills</Link>
            <Link  className='Navbar-menu_btn btn btn-info'>Contacto</Link>
        </div>
     </div>
    );
}

export default NavBar;