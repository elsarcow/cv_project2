import React from 'react';
import './styles/App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Layout from './Layout';
import Home from '../pages/Home'
import Bio from '../pages/Bio';
import Skills from '../pages/Skills';

function App() {
  return (
    <BrowserRouter>
    <Layout>
    <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/bio" component={Bio}/>
        <Route exact path="/skills" component={Skills}/>
     </Switch>
    </Layout>
 </BrowserRouter> 
  );
}

export default App;
