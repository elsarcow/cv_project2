import React from "react";

import "../pages/styles/Bio.css";
import "bootstrap/dist/css/bootstrap.css";

function Bio() {
  return (
    <React.Fragment>
      <div className="container">
        <div className="row">
          <div className="col-4">
            <div className="Hero">
                <img className="Hero__image" src={"http://placekitten.com/g/200/300"} alt="hero" />
            </div>
          </div>
          <div className="col-8">
            <h1>BIO</h1>
            <h2>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium ipsam porro accusamus corporis dolorum fugit facilis nesciunt cupiditate reiciendis commodi. Rem laborum eaque non dolore beatae dolorem nostrum ad facilis!
            </h2>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default Bio;
