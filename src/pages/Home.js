import React from 'react';
import './styles/Home.css'

import {Link} from 'react-router-dom'

import ProfilePicture from '../components/ProfilePicture'

function Home(){
    return (
        <React.Fragment>
            <div className='container Home-container'>
                <div className='row '>
                    <div className='col Home_profile-pic'>
                    <ProfilePicture className=''/>
                    </div>
                    <div className='col-6'>
                        <div className='Home_profile-buttons_menu'>
                            <Link to='/bio' className='Home_profile-menu_btn btn btn-dark btn-lg btn-block'>Bio</Link>
                            <Link to='/skills' className='Home_profile-menu_btn btn btn-dark btn-lg btn-block'>Skills</Link>
                            <Link to='/skills' className='Home_profile-menu_btn btn btn-dark btn-lg btn-block'>Contacto</Link>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Home;