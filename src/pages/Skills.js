import React from "react";

import "../pages/styles/Skills.css";
import "bootstrap/dist/css/bootstrap.css";

function Skills() {
  return (
    <React.Fragment>
      <div className="container">
        <h1>  <strong>Habilidades</strong></h1>

        <div>
          <strong>Diseño Grafico</strong>
          <div class="barConteiner progress">
            <div class="skills bar1">
              <strong>90%</strong>
            </div>
          </div>

          <strong>Diseño Web</strong>
          <div class="barConteiner progress">
            <div class="skills bar2">
              <strong>80%</strong>
            </div>
          </div>

          <strong>React JS</strong>
          <div class="barConteiner progress">
            <div class="skills bar3">
              <strong>65%</strong>
            </div>
          </div>

          <strong>Node JS</strong>
          <div class="barConteiner progress">
            <div class="skills bar4">
              <strong>60%</strong>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default Skills;
